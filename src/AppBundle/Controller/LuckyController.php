<?php

// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LuckyController extends Controller
{
    /**
     * @Route("/lucky/number")
     */
    // public function numberAction()
    // {
    //     $number = mt_rand(0, 100);

    //     return new Response(
    //         '<html>
    //         <title>Practice Getting Lucky</title>
    //         <body>
    //             <div><span style="color:red;font-size:5em;">Lucky number: '.$number.'</span></div>
    //         </body>
    //         </html>'
    //     );
    // }

    // public function numberAction()
    // {
    //     $number = mt_rand(0, 100);

    //     return $this->render('lucky/number.html.twig', array(
    //         'number' => $number,
    //     ));
    // }

    /**
     * @Route("/lucky/number/{max}")
     */
    public function numberAction($max)
    {
        $number = mt_rand(0, $max);

        return new Response(
            '<html>
                <body>
                    <div><span style="color:orange;font-size:5em;">Lucky number: '.$number.'</span></div>
                </body>
            </html>'
        );
    }

}