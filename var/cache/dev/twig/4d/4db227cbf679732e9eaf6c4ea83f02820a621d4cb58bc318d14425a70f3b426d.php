<?php

/* lucky/number.html.twig */
class __TwigTemplate_45a4b7446787fea86723cec543902da85cc894fbbf9f848af067f15834ec1948 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8d36597f489c0f0decaf7824495050045dde28e5f0aa80ea5737837f7b06514 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8d36597f489c0f0decaf7824495050045dde28e5f0aa80ea5737837f7b06514->enter($__internal_a8d36597f489c0f0decaf7824495050045dde28e5f0aa80ea5737837f7b06514_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "lucky/number.html.twig"));

        $__internal_0ba486e929d97c56a45037f1425788a40b203c91d0d31241730565d9fed3c8a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ba486e929d97c56a45037f1425788a40b203c91d0d31241730565d9fed3c8a7->enter($__internal_0ba486e929d97c56a45037f1425788a40b203c91d0d31241730565d9fed3c8a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "lucky/number.html.twig"));

        // line 2
        echo "
<h1 style=\"color:blue;font-size:5em;\">Your lucky number is ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["number"]) ? $context["number"] : $this->getContext($context, "number")), "html", null, true);
        echo "</h1>

";
        
        $__internal_a8d36597f489c0f0decaf7824495050045dde28e5f0aa80ea5737837f7b06514->leave($__internal_a8d36597f489c0f0decaf7824495050045dde28e5f0aa80ea5737837f7b06514_prof);

        
        $__internal_0ba486e929d97c56a45037f1425788a40b203c91d0d31241730565d9fed3c8a7->leave($__internal_0ba486e929d97c56a45037f1425788a40b203c91d0d31241730565d9fed3c8a7_prof);

    }

    public function getTemplateName()
    {
        return "lucky/number.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/lucky/number.html.twig #}

<h1 style=\"color:blue;font-size:5em;\">Your lucky number is {{ number }}</h1>

", "lucky/number.html.twig", "/Users/Home/Documents/Julseyong/TestingOne/app/Resources/views/lucky/number.html.twig");
    }
}
