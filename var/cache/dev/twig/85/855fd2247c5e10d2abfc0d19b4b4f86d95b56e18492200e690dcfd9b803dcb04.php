<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_3a7de430e9e6534dda7f831c709cb9df3813ac7d033045c83d8e4d4b40d9a3ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8db1217b08398b1ed0473625a2e87bfe7b411ecc0ebf98a35f3f85ce4202eac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8db1217b08398b1ed0473625a2e87bfe7b411ecc0ebf98a35f3f85ce4202eac->enter($__internal_d8db1217b08398b1ed0473625a2e87bfe7b411ecc0ebf98a35f3f85ce4202eac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_00398d9e5f960560bb4afa0f9c91f803829eba308860ef189069903f2ceb6075 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00398d9e5f960560bb4afa0f9c91f803829eba308860ef189069903f2ceb6075->enter($__internal_00398d9e5f960560bb4afa0f9c91f803829eba308860ef189069903f2ceb6075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8db1217b08398b1ed0473625a2e87bfe7b411ecc0ebf98a35f3f85ce4202eac->leave($__internal_d8db1217b08398b1ed0473625a2e87bfe7b411ecc0ebf98a35f3f85ce4202eac_prof);

        
        $__internal_00398d9e5f960560bb4afa0f9c91f803829eba308860ef189069903f2ceb6075->leave($__internal_00398d9e5f960560bb4afa0f9c91f803829eba308860ef189069903f2ceb6075_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_17e6975535f70d7d2aa7305e2c15d39d20553876f805b844dc7b326fb906a7ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17e6975535f70d7d2aa7305e2c15d39d20553876f805b844dc7b326fb906a7ba->enter($__internal_17e6975535f70d7d2aa7305e2c15d39d20553876f805b844dc7b326fb906a7ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_1b7d06f7762eb6923d7eda4766e98be7052e141e450876ee8cf80b53d773604f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b7d06f7762eb6923d7eda4766e98be7052e141e450876ee8cf80b53d773604f->enter($__internal_1b7d06f7762eb6923d7eda4766e98be7052e141e450876ee8cf80b53d773604f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_1b7d06f7762eb6923d7eda4766e98be7052e141e450876ee8cf80b53d773604f->leave($__internal_1b7d06f7762eb6923d7eda4766e98be7052e141e450876ee8cf80b53d773604f_prof);

        
        $__internal_17e6975535f70d7d2aa7305e2c15d39d20553876f805b844dc7b326fb906a7ba->leave($__internal_17e6975535f70d7d2aa7305e2c15d39d20553876f805b844dc7b326fb906a7ba_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_67b2b2e642c90ab19b02a46f29262635046fa09fce6bd7e9115d6e9917054c9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67b2b2e642c90ab19b02a46f29262635046fa09fce6bd7e9115d6e9917054c9f->enter($__internal_67b2b2e642c90ab19b02a46f29262635046fa09fce6bd7e9115d6e9917054c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2365f4071bb2b89e8e22d0a74f464469c3f8929e5bbdf48cfb42e5b0a40cc7dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2365f4071bb2b89e8e22d0a74f464469c3f8929e5bbdf48cfb42e5b0a40cc7dc->enter($__internal_2365f4071bb2b89e8e22d0a74f464469c3f8929e5bbdf48cfb42e5b0a40cc7dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_2365f4071bb2b89e8e22d0a74f464469c3f8929e5bbdf48cfb42e5b0a40cc7dc->leave($__internal_2365f4071bb2b89e8e22d0a74f464469c3f8929e5bbdf48cfb42e5b0a40cc7dc_prof);

        
        $__internal_67b2b2e642c90ab19b02a46f29262635046fa09fce6bd7e9115d6e9917054c9f->leave($__internal_67b2b2e642c90ab19b02a46f29262635046fa09fce6bd7e9115d6e9917054c9f_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9d499cc4c27a55c05f30123db97dfdde03f0321e3b4cd14600caab49193b66ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d499cc4c27a55c05f30123db97dfdde03f0321e3b4cd14600caab49193b66ca->enter($__internal_9d499cc4c27a55c05f30123db97dfdde03f0321e3b4cd14600caab49193b66ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_1d41a475236670d00e13c3dbc4cd53b395415ec80dd9796108ad2532a3bd765c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d41a475236670d00e13c3dbc4cd53b395415ec80dd9796108ad2532a3bd765c->enter($__internal_1d41a475236670d00e13c3dbc4cd53b395415ec80dd9796108ad2532a3bd765c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_1d41a475236670d00e13c3dbc4cd53b395415ec80dd9796108ad2532a3bd765c->leave($__internal_1d41a475236670d00e13c3dbc4cd53b395415ec80dd9796108ad2532a3bd765c_prof);

        
        $__internal_9d499cc4c27a55c05f30123db97dfdde03f0321e3b4cd14600caab49193b66ca->leave($__internal_9d499cc4c27a55c05f30123db97dfdde03f0321e3b4cd14600caab49193b66ca_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/Users/Home/Documents/Julseyong/TestingOne/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
