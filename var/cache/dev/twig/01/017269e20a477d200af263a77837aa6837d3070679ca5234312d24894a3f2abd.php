<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_93dcf1a9f93b7677956a1e6986664e626fde0dea5fd8969ecb378cfa9bb65a54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_74d3574c3e98cc2b4c53fc9e7eab38df3f25aeb9e3d42ccebe792c2d75cc48ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74d3574c3e98cc2b4c53fc9e7eab38df3f25aeb9e3d42ccebe792c2d75cc48ec->enter($__internal_74d3574c3e98cc2b4c53fc9e7eab38df3f25aeb9e3d42ccebe792c2d75cc48ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_e3a2da53b9d7270da79480b1bbb6fe02195d9c406a0c801a8abecbe83c691f9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3a2da53b9d7270da79480b1bbb6fe02195d9c406a0c801a8abecbe83c691f9b->enter($__internal_e3a2da53b9d7270da79480b1bbb6fe02195d9c406a0c801a8abecbe83c691f9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_74d3574c3e98cc2b4c53fc9e7eab38df3f25aeb9e3d42ccebe792c2d75cc48ec->leave($__internal_74d3574c3e98cc2b4c53fc9e7eab38df3f25aeb9e3d42ccebe792c2d75cc48ec_prof);

        
        $__internal_e3a2da53b9d7270da79480b1bbb6fe02195d9c406a0c801a8abecbe83c691f9b->leave($__internal_e3a2da53b9d7270da79480b1bbb6fe02195d9c406a0c801a8abecbe83c691f9b_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/Users/Home/Documents/Julseyong/TestingOne/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
