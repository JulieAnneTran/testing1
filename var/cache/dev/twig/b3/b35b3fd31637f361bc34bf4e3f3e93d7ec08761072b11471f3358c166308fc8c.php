<?php

/* base.html.twig */
class __TwigTemplate_7c3f8a4eb1d06ea601c66eec653a7c881d59a42c4f5067d5457f74ff96a849ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_098871b2e7a4ebd0ff63f9a15cd9b28f7e3cc8fbbdfcf5e40a66d5d2ea6036d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_098871b2e7a4ebd0ff63f9a15cd9b28f7e3cc8fbbdfcf5e40a66d5d2ea6036d1->enter($__internal_098871b2e7a4ebd0ff63f9a15cd9b28f7e3cc8fbbdfcf5e40a66d5d2ea6036d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_ce9163f2d326f4c4dd4cb9989976300b4496127cdf0481d3d0915b8808fde1c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce9163f2d326f4c4dd4cb9989976300b4496127cdf0481d3d0915b8808fde1c4->enter($__internal_ce9163f2d326f4c4dd4cb9989976300b4496127cdf0481d3d0915b8808fde1c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_098871b2e7a4ebd0ff63f9a15cd9b28f7e3cc8fbbdfcf5e40a66d5d2ea6036d1->leave($__internal_098871b2e7a4ebd0ff63f9a15cd9b28f7e3cc8fbbdfcf5e40a66d5d2ea6036d1_prof);

        
        $__internal_ce9163f2d326f4c4dd4cb9989976300b4496127cdf0481d3d0915b8808fde1c4->leave($__internal_ce9163f2d326f4c4dd4cb9989976300b4496127cdf0481d3d0915b8808fde1c4_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_0214319cab0941b17ade9c03f9d7a3b5e552b0d12ba00f36edfdec13c8cb5d28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0214319cab0941b17ade9c03f9d7a3b5e552b0d12ba00f36edfdec13c8cb5d28->enter($__internal_0214319cab0941b17ade9c03f9d7a3b5e552b0d12ba00f36edfdec13c8cb5d28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_59e78003e65ea0a0cfc7b11841dc7ced12f224abdf7a3129bc31686d5647ad97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59e78003e65ea0a0cfc7b11841dc7ced12f224abdf7a3129bc31686d5647ad97->enter($__internal_59e78003e65ea0a0cfc7b11841dc7ced12f224abdf7a3129bc31686d5647ad97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_59e78003e65ea0a0cfc7b11841dc7ced12f224abdf7a3129bc31686d5647ad97->leave($__internal_59e78003e65ea0a0cfc7b11841dc7ced12f224abdf7a3129bc31686d5647ad97_prof);

        
        $__internal_0214319cab0941b17ade9c03f9d7a3b5e552b0d12ba00f36edfdec13c8cb5d28->leave($__internal_0214319cab0941b17ade9c03f9d7a3b5e552b0d12ba00f36edfdec13c8cb5d28_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b18ba12e55ab69b241e37ca71b11c61f9e5f7501002593ecf9dadad0308d45d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b18ba12e55ab69b241e37ca71b11c61f9e5f7501002593ecf9dadad0308d45d6->enter($__internal_b18ba12e55ab69b241e37ca71b11c61f9e5f7501002593ecf9dadad0308d45d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_c2318b5c18d75bb394499987773f9db8cb990c237b5745b57764c92d12f90919 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2318b5c18d75bb394499987773f9db8cb990c237b5745b57764c92d12f90919->enter($__internal_c2318b5c18d75bb394499987773f9db8cb990c237b5745b57764c92d12f90919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_c2318b5c18d75bb394499987773f9db8cb990c237b5745b57764c92d12f90919->leave($__internal_c2318b5c18d75bb394499987773f9db8cb990c237b5745b57764c92d12f90919_prof);

        
        $__internal_b18ba12e55ab69b241e37ca71b11c61f9e5f7501002593ecf9dadad0308d45d6->leave($__internal_b18ba12e55ab69b241e37ca71b11c61f9e5f7501002593ecf9dadad0308d45d6_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_71bf131bcb14466e0d4bdacd255eabe4b31508d93ef14072e5a1b49da764a9d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71bf131bcb14466e0d4bdacd255eabe4b31508d93ef14072e5a1b49da764a9d2->enter($__internal_71bf131bcb14466e0d4bdacd255eabe4b31508d93ef14072e5a1b49da764a9d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3c312f7bd35e82cb1e0cbebe66beeb80367768bebc050eb81ed6f272b11e3917 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c312f7bd35e82cb1e0cbebe66beeb80367768bebc050eb81ed6f272b11e3917->enter($__internal_3c312f7bd35e82cb1e0cbebe66beeb80367768bebc050eb81ed6f272b11e3917_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_3c312f7bd35e82cb1e0cbebe66beeb80367768bebc050eb81ed6f272b11e3917->leave($__internal_3c312f7bd35e82cb1e0cbebe66beeb80367768bebc050eb81ed6f272b11e3917_prof);

        
        $__internal_71bf131bcb14466e0d4bdacd255eabe4b31508d93ef14072e5a1b49da764a9d2->leave($__internal_71bf131bcb14466e0d4bdacd255eabe4b31508d93ef14072e5a1b49da764a9d2_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b60d418b0e5e71cd55f5ebd0547f1a4be92caa79ba4764eca68715ee36bbf929 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b60d418b0e5e71cd55f5ebd0547f1a4be92caa79ba4764eca68715ee36bbf929->enter($__internal_b60d418b0e5e71cd55f5ebd0547f1a4be92caa79ba4764eca68715ee36bbf929_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_0070ed90620d582d350f76b8e95b4a2f907bb1da7c70c09da07d14a9e7cb3b21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0070ed90620d582d350f76b8e95b4a2f907bb1da7c70c09da07d14a9e7cb3b21->enter($__internal_0070ed90620d582d350f76b8e95b4a2f907bb1da7c70c09da07d14a9e7cb3b21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_0070ed90620d582d350f76b8e95b4a2f907bb1da7c70c09da07d14a9e7cb3b21->leave($__internal_0070ed90620d582d350f76b8e95b4a2f907bb1da7c70c09da07d14a9e7cb3b21_prof);

        
        $__internal_b60d418b0e5e71cd55f5ebd0547f1a4be92caa79ba4764eca68715ee36bbf929->leave($__internal_b60d418b0e5e71cd55f5ebd0547f1a4be92caa79ba4764eca68715ee36bbf929_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/Users/Home/Documents/Julseyong/TestingOne/app/Resources/views/base.html.twig");
    }
}
