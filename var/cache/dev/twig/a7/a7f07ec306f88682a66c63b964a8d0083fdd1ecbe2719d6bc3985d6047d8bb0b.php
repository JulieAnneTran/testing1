<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_812bd81da33ffd6ff59aafdab11a467dc6a3768b505c1dab12f729eca2f0c262 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c13913e0524df96a49f1f78600543089b6aef560af0b7849b4057dab5cea9b95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c13913e0524df96a49f1f78600543089b6aef560af0b7849b4057dab5cea9b95->enter($__internal_c13913e0524df96a49f1f78600543089b6aef560af0b7849b4057dab5cea9b95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_06b0d7f368b277e53626e60e408b4fc5278d027ada65c6dbe9ae414f02b4cef9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06b0d7f368b277e53626e60e408b4fc5278d027ada65c6dbe9ae414f02b4cef9->enter($__internal_06b0d7f368b277e53626e60e408b4fc5278d027ada65c6dbe9ae414f02b4cef9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c13913e0524df96a49f1f78600543089b6aef560af0b7849b4057dab5cea9b95->leave($__internal_c13913e0524df96a49f1f78600543089b6aef560af0b7849b4057dab5cea9b95_prof);

        
        $__internal_06b0d7f368b277e53626e60e408b4fc5278d027ada65c6dbe9ae414f02b4cef9->leave($__internal_06b0d7f368b277e53626e60e408b4fc5278d027ada65c6dbe9ae414f02b4cef9_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_eff68f066cc18b922e78b27175f59cdfef9cf8050333fb00354f384b4800837a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eff68f066cc18b922e78b27175f59cdfef9cf8050333fb00354f384b4800837a->enter($__internal_eff68f066cc18b922e78b27175f59cdfef9cf8050333fb00354f384b4800837a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_ca5be1ad6299f8d2f4c9d4da68e1cde8b3c59963a7ebdc131962b84a35f90195 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca5be1ad6299f8d2f4c9d4da68e1cde8b3c59963a7ebdc131962b84a35f90195->enter($__internal_ca5be1ad6299f8d2f4c9d4da68e1cde8b3c59963a7ebdc131962b84a35f90195_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_ca5be1ad6299f8d2f4c9d4da68e1cde8b3c59963a7ebdc131962b84a35f90195->leave($__internal_ca5be1ad6299f8d2f4c9d4da68e1cde8b3c59963a7ebdc131962b84a35f90195_prof);

        
        $__internal_eff68f066cc18b922e78b27175f59cdfef9cf8050333fb00354f384b4800837a->leave($__internal_eff68f066cc18b922e78b27175f59cdfef9cf8050333fb00354f384b4800837a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_3f0d10bd89c396a070a8c47f9976f9b30ae3b1c985a83d90fdb55d3dad3ceb59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f0d10bd89c396a070a8c47f9976f9b30ae3b1c985a83d90fdb55d3dad3ceb59->enter($__internal_3f0d10bd89c396a070a8c47f9976f9b30ae3b1c985a83d90fdb55d3dad3ceb59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_30750dbee9832bf153c6216864eeec8d05e92cfb8e9f9150613faaf73ce40beb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30750dbee9832bf153c6216864eeec8d05e92cfb8e9f9150613faaf73ce40beb->enter($__internal_30750dbee9832bf153c6216864eeec8d05e92cfb8e9f9150613faaf73ce40beb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_30750dbee9832bf153c6216864eeec8d05e92cfb8e9f9150613faaf73ce40beb->leave($__internal_30750dbee9832bf153c6216864eeec8d05e92cfb8e9f9150613faaf73ce40beb_prof);

        
        $__internal_3f0d10bd89c396a070a8c47f9976f9b30ae3b1c985a83d90fdb55d3dad3ceb59->leave($__internal_3f0d10bd89c396a070a8c47f9976f9b30ae3b1c985a83d90fdb55d3dad3ceb59_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f0747f3c287e384ed03ade4bb9bafec3d1517559edffff06bef5f5554202fa39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0747f3c287e384ed03ade4bb9bafec3d1517559edffff06bef5f5554202fa39->enter($__internal_f0747f3c287e384ed03ade4bb9bafec3d1517559edffff06bef5f5554202fa39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_a6a9db5ae17691746bf680c79f4d62c5a7b078aae46ac53a0091f44c444d84b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6a9db5ae17691746bf680c79f4d62c5a7b078aae46ac53a0091f44c444d84b1->enter($__internal_a6a9db5ae17691746bf680c79f4d62c5a7b078aae46ac53a0091f44c444d84b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a6a9db5ae17691746bf680c79f4d62c5a7b078aae46ac53a0091f44c444d84b1->leave($__internal_a6a9db5ae17691746bf680c79f4d62c5a7b078aae46ac53a0091f44c444d84b1_prof);

        
        $__internal_f0747f3c287e384ed03ade4bb9bafec3d1517559edffff06bef5f5554202fa39->leave($__internal_f0747f3c287e384ed03ade4bb9bafec3d1517559edffff06bef5f5554202fa39_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/Users/Home/Documents/Julseyong/TestingOne/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
